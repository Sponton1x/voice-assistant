# Voice Assistant 🤖
Voice Assistant has been written in Python 🐍 using PyAudio, SpeechRecognition, Pyttsx3 <br> <br>
    <a href="https://www.python.org/">
    	<img src="https://img.shields.io/badge/built%20with-Python3-red.svg" />
    </a> <br>
Features:
* open webrowser
* open email
* open facebook
* open github
* say weather
* say time
* lock screen
* turn off pc
* search wikipedia
* introduce yourself

## What I used to:
- [x] Pyttsx3 ([Docs](https://pyttsx3.readthedocs.io/en/latest/))<br>
- [X] SpeechRecognition ([Docs](https://developer.mozilla.org/en-US/docs/Web/API/SpeechRecognition)) <br>
- [X] PyAudio

## Installation:

     Linux/macOS
    git clone https://github.com/Sponton1x/AI.git
    cd Voice Assistant
    python3 main.py

     Windows
    git clone https://github.com/Sponton1x/AI.git
    cd Voice Assistant
    main.py
    
## Install PyAudio:

    Windows
    pip install pipwin
    pipwin install pyaudio
    
    Linux/macOS
    python3 pip install pipwin
    pipwin install pyaudio
    
## Getting Started
Install [Python](https://www.python.org/downloads/) ``recommend`` <br>
Login on this [website](https://openweathermap.org/api) and get api key into a tab and paste in api_keys.txt

    Install requirements
    pip install -r requirements.txt

## Help
  If you have a probrem with this repository you can ask me on Discord (Sponton#4170) 
